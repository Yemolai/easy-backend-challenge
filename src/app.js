require('dotenv').config()
const express = require('express')
const CORS = require('cors')
const jwt = require('express-jwt')
const { JWT_SECRET: secret = 'supersecretsecret' } = process.env

const logger = require('./logger')
const indexRouter = require('./routes/index')
const loginRouter = require('./routes/login')
const swaggerRouter = require('./routes/swagger')

const cors = CORS({
  origin: '*',
  optionsSuccessStatus: 200
})

const app = express()

app.use(cors)
app.use(logger)
app.use(express.json())

/* Unprotected routes */
app.use('/', indexRouter)
app.use('/login', loginRouter)
app.use('/swagger', swaggerRouter)

app.use(jwt({ secret }))

app.use((err, _req, res, _next) => {
  if (err.name === 'UnauthorizedError') {
    res.status(401).json({
      error: true,
      message: 'invalid-or-missing-token'
    })
  }
  res.status(500).json({
    error: true,
    message: 'internal-error'
  })
})

/* Protected routes */
app.use('/partners', require('./routes/partners'))
app.use('/services', require('./routes/services'))

app.use('**', (_req, res) => res.status(404).json({ error: true, message: 'not-found' }))

module.exports = app
