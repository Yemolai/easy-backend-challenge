const router = require('express').Router()
const axios = require('axios')
const geolib = require('geolib')
const { sequelize } = require('../database')

const { GMAPS_TOKEN } = process.env

if (!GMAPS_TOKEN) {
  console.warn('No GMAPS_TOKEN was found, will not be able to use the geocoding service')
}

// partner should be within 10.000m radius of distance
const rangeInMeters = process.env.RANGE || 10000

const defaultGMapsGeocodingAPIURL = 'https://maps.googleapis.com/maps/api/geocode/json'

const geocodeServiceQueryByAddress = address => {
  const url = process.env.GEOCODING_API_URL || defaultGMapsGeocodingAPIURL
  const queryParams = new URLSearchParams({
    key: GMAPS_TOKEN,
    address: `${address}`
  })
  return axios.get(`${url}?${queryParams}`)
}

/**
 * @swagger
 *
 * /partners/near:
 *  get:
 *    description: partners list
 *    parameters:
 *      - $ref: '#/components/parameters/addressQueryParam'
 *        required: true
 *    responses:
 *      200:
 *        description: success
 *        in: body
 *        schema:
 *          type: object
 *          properties:
 *            error:
 *              type: boolean
 *            data:
 *              type: array
 *              items:
 *                properties:
 *                  id:
 *                    type: integer
 *                  name:
 *                    type: string
 *      401:
 *        $ref: '#/components/responses/Unauthorized'
 *      500:
 *        $ref: '#/components/responses/InternalFailure'
 */
router.get('/near', async (req, res) => {
  if (!GMAPS_TOKEN) {
    return res.status(500).json({ error: true, message: 'internal-error' })
  }
  try {
    const { partner: Partner, location: Location, service: Service } = sequelize.models
    const { address } = req.query
    const { data: response } = await geocodeServiceQueryByAddress(address)
      .catch(err => {
        throw err
      })
    if (response.results.length < 1) {
      return res.status(200).json({ error: true, message: 'invalid-address' })
    }
    const result = response.results.find(() => true)
    const { location: geocodingLocation } = result.geometry
    const userLocation = {
      latitude: geocodingLocation.lat,
      longitude: geocodingLocation.lng
    }

    // This could be hugely improved with usage of PostGIS
    // to query geolocations directly from a table index
    // instead of querying the whole partner list to filter
    const partners = await Partner.findAll({
      attributes: {
        exclude: ['createdAt', 'updatedAt', 'locationId']
      },
      include: [
        {
          model: Service,
          as: 'availableServices',
          attributes: ['name']
        },
        {
          model: Location,
          as: 'location',
          attributes: {
            exclude: ['id', 'createdAt', 'updatedAt']
          }
        }
      ]
    })

    const partnersInRange = partners.filter(partner => {
      const partnerLocation = {
        latitude: partner.location.lat,
        longitude: partner.location.long
      }
      return geolib.isPointWithinRadius(partnerLocation, userLocation, rangeInMeters)
    })

    // removing relation unneded data from response
    const serializedPartners = JSON.stringify(partnersInRange)
    const partnerList = JSON.parse(serializedPartners)
      .map(partner => ({
        ...partner,
        availableServices: partner.availableServices.map(({ name }) => name),
        partnerServices: undefined
      }))

    return res.status(200).json({ error: false, data: partnerList })
  } catch (err) {
    console.error(err.message, err)
    return res.status(500).json({ error: true, message: 'internal-error' })
  }
})

module.exports = router
