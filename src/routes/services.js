const router = require('express').Router()
const geolib = require('geolib')
const { sequelize } = require('../database')

// partner should be within 10.000m radius of distance
const rangeInMeters = process.env.RANGE || 10000

/**
 * @swagger
 *
 * /services:
 *   get:
 *    description: available services list
 *    produces:
 *      application/json
 *    responses:
 *      200:
 *        description: success
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                error:
 *                  type: boolean
 *                data:
 *                  type: array
 *                  $ref: '#/components/schemas/ArrayOfStrings'
 *      401:
 *        $ref: '#/components/responses/Unauthorized'
 *      500:
 *        $ref: '#/components/responses/InternalFailure'
 */
router.get('/', async (_req, res) => {
  const { service: Service } = sequelize.models
  const services = await Service.findAll()
  const servicesNames = services.map(service => service.name)
  return res.status(200).json({ error: false, data: servicesNames })
})

/**
 * @swagger
 *
 * /services/{service}/partners/nearest:
 *  get:
 *    description: find nearest partner provider of service
 *    parameters:
 *      - name: service
 *        in: path
 *        required: true
 *        example: OIL_CHANGE
 *      - $ref: '#/components/parameters/latitudeQueryParam'
 *        required: true
 *      - $ref: '#/components/parameters/longitudeQueryParam'
 *        required: true
 *    responses:
 *      200:
 *        description: success
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                error:
 *                  type: boolean
 *                  example: false
 *                data:
 *                  type: object
 *                  $ref: '#/components/schemas/Partner'
 *      401:
 *        $ref: '#/components/responses/Unauthorized'
 *      500:
 *        $ref: '#/components/responses/InternalFailure'
 */
router.get('/:service/partners/nearest', async (req, res) => {
  const { service } = req.params
  const { lat, long } = req.query
  const requestedService = `${service}`.toUpperCase()

  const invalid = [
    isNaN(Number(lat)) && 'latitude',
    isNaN(Number(long)) && 'longitude'
  ].filter(v => v)

  if (invalid.length) {
    const code = 'invalid-' + invalid.join('-')
    return res.status(400).json({ error: true, code })
  }

  const userGeolocation = {
    latitude: lat,
    longitude: long
  }

  try {
    const {
      service: Service,
      partner: Partner,
      location: Location
    } = sequelize.models
    // This could be hugely improved with usage of PostGIS
    // to query geolocations directly from a table index
    // instead of querying the whole partner list to filter
    const serviceProviders = await Service.findOne({
      where: {
        name: requestedService
      },
      include: [
        {
          model: Partner,
          as: 'partners',
          attributes: {
            exclude: ['createdAt', 'updatedAt', 'locationId']
          },
          include: [
            {
              model: Service,
              as: 'availableServices',
              attributes: ['name']
            },
            {
              model: Location,
              as: 'location',
              attributes: {
                exclude: ['id', 'createdAt', 'updatedAt']
              }
            }
          ]
        }
      ]
    })
    const partnersGeolocations = serviceProviders.partners.map(partner => {
      const location = {
        id: partner.id,
        latitude: partner.location.lat,
        longitude: partner.location.long
      }
      return location
    })
    const nearestPartnerLocation = geolib
      .findNearest(userGeolocation, partnersGeolocations)

    const isPartnerCloseEnough = geolib
      .isPointWithinRadius(nearestPartnerLocation, userGeolocation, rangeInMeters)

    if (!isPartnerCloseEnough) {
      return res.status(200).json({ error: true, message: 'no-available-partner-around' })
    }

    const nearestPartner = serviceProviders.partners
      .find(partner => partner.id === nearestPartnerLocation.id)

    // removing relation unneded data from response
    const serializedPartner = JSON.stringify(nearestPartner)
    const rebuiltPartner = JSON.parse(serializedPartner)
    const selectedPartner = {
      ...rebuiltPartner,
      availableServices: rebuiltPartner.availableServices.map(({ name }) => name),
      partnerServices: undefined
    }

    return res.status(200).json({ error: false, data: selectedPartner })
  } catch (err) {
    console.debug('failed at service request', [err.message, err])
    return res.status(500).json({ error: true, message: 'internal-error' })
  }
})

module.exports = router
