var express = require('express');
var router = express.Router();

/**
 * @swagger
 * /:
 *  get:
 *    description: redirects to swagger ui page
 *    security: []
 *    responses:
 *      301:
 *        description: redirect to swagger ui page
 */
router.get('/', function(_, res) {
  res.status(301).redirect('/swagger');
});

module.exports = router;
