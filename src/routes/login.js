const express = require('express')
const jsonwebtoken = require('jsonwebtoken')
const router = express.Router()
const { sequelize } = require('../database')
const { JWT_SECRET: secret = 'supersecretsecret' } = process.env

/**
 * @swagger
 *
 * /login:
 *  post:
 *    description: login into the application
 *    security: []
 *    consumes:
 *      - application/json
 *    produces:
 *      - application/json
 *    parameters:
 *      - $ref: '#/components/parameters/credentials'
 *    responses:
 *      200:
 *        description: login
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                error:
 *                  type: boolean
 *                  example: false
 *                data:
 *                  $ref: '#/components/schemas/AuthenticationResponse'
 *      400:
 *        $ref: '#/components/responses/AuthenticationInvalidPayloadError'
 *      401:
 *        $ref: '#/components/responses/AuthenticationWrongCredentialsError'
 *      500:
 *        $ref: '#/components/responses/InternalFailure'
 */
router.post('/', async (req, res) => {
  try {
    const { email, password } = req.body
    console.log({ email, password })
    const { user: UserModel } = sequelize.models
    if (!email || !password) {
      return res.status(400).json({ error: true, message: 'invalid-payload' })
    }
    const user = await UserModel.findOne({
      where: { email, password },
      attributes: ['id', 'name', 'email']
    })
    if (user === null) {
      return res.status(401).json({ error: true, message: 'invalid-credentials' })
    }
    const payload = { user }
    const token = jsonwebtoken.sign(payload, secret)
    const data = {
      token
    }
    return res.status(200).json({ error: false, data })
  } catch (err) {
    console.log({ secret })
    console.error(err.message, err)
    return res.status(500).json({ error: true, message: 'internal-error' })
  }
})

module.exports = router
