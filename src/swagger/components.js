module.exports = {
  securitySchemes: {
    Bearer: {
      type: 'http',
      scheme: 'bearer',
      bearerFormat: 'JWT',
    }
  },
  parameters: {
    credentials: {
      in: 'body',
      description: 'user credentials',
      schema: {
        $ref: '#/components/schemas/AuthenticationPayload'
      }
    },
    addressQueryParam: {
      name: 'address',
      in: 'query',
      example: 'R. Gonçalo Afonso - Vila Madalena, São Paulo'
    },
    latitudeQueryParam: {
      name: 'lat',
      in: 'query',
      example: -3.10195
    },
    longitudeQueryParam: {
      name: 'long',
      in: 'query',
      example: -60.11
    }
  },
  schemas: {
    AuthenticationResponse: {
      type: 'object',
      properties: {
        token: {
          type: 'string',
          example: 'eyJhbGciOI6IkpXVCJ9.eyJ1c2VyIjp7...',
          description: 'generated token',
        }
      }
    },
    ArrayOfStrings: {
      type: 'array',
      items: {
        type: 'string'
      }
    },
    AuthenticationPayload: {
      type: 'object',
      properties: {
        email: {
          type: 'string'
        },
        password: {
          type: 'string'
        }
      }
    },
    Location: {
      type: 'object',
      properties: {
        name: {
          type: 'string'
        },
        address: {
          type: 'string'
        },
        city: {
          type: 'string'
        },
        state: {
          type: 'string'
        },
        country: {
          type: 'string'
        },
        lat: {
          type: 'number'
        },
        long: {
          type: 'number'
        }
      }
    },
    Partner: {
      type: 'object',
      properties: {
        id: {
          type: 'string'
        },
        name: {
          type: 'string'
        },
        location: {
          $ref: '#/components/schemas/Location'
        },
        availableServices: {
          type: 'array',
          $ref: '#/components/schemas/ArrayOfStrings'
        }
      }
    },
    Unauthorized: {
      type: 'object',
      properties: {
        error: {
          type: 'boolean',
          example: true
        },
        message: {
          type: 'string',
          example: 'invalid-or-missing-token'
        }
      }
    },
    InternalError: {
      type: 'object',
      properties: {
        error: {
          type: 'boolean',
          example: true
        },
        message: {
          type: 'string',
          example: 'internal-error'
        }
      }
    }
  },
  responses: {
    AuthenticationInvalidPayloadError: {
      description: 'invalid post body payload',
      content: {
        'application/json': {
          schema: {
            properties: {
              error: {
                type: 'boolean',
                example: true
              },
              message: {
                type: 'string',
                example: 'invalid-payload'
              }
            }
          }
        }
      }
    },
    AuthenticationWrongCredentialsError: {
      description: 'wrong credentials',
      content: {
        'application/json': {
          schema: {
            properties: {
              error: {
                type: 'boolean',
                example: true
              },
              message: {
                type: 'string',
                example: 'wrong-credentials'
              }
            }
          }
        }
      }
    },
    Unauthorized: {
      description: 'unauthorized',
      content: {
        'application/json': {
          schema: {
            type: 'object',
            $ref: '#/components/schemas/Unauthorized'
          }
        }
      }
    },
    InternalFailure: {
      description: 'internal failure',
      content: {
        'application/json': {
          schema: {
            type: 'object',
            $ref: '#/components/schemas/InternalError'
          }
        }
      }
    }
  }
}
