const swaggerJSDoc = require('swagger-jsdoc')
const { version, description } = require('../../package.json')
const components = require('./components')
const { HOST: host = 'localhost', PORT: port = 8080 } = process.env

const swaggerDefinition = {
  info: {
    title: 'Easy carros backend challenge',
    version,
    description
  },
  openapi: '3.0.1',
  /* eslint-disable-next-line eqeqeq */
  host: port == 80 ? host : `${host}:${port}`,
  basePath: '/',
  components,
  security: [{
    Bearer: []
  }]
}

const options = {
  swaggerDefinition,
  apis: ['src/routes/*.js'],
  explorer: true
}

const swaggerSpec = swaggerJSDoc(options)

module.exports = swaggerSpec
