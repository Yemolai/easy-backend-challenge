'use strict'

module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.sequelize.transaction((transaction) =>
      queryInterface.addColumn('partners', 'locationId', {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: 'locations',
            key: 'id'
          },
          onUpdate: 'cascade'
        }
      }, { transaction })
    ),
  down: (queryInterface, _) =>
    queryInterface.sequelize.transaction((transaction) =>
      queryInterface.removeColumn('partners', 'locationId', { transaction })
    )
}
