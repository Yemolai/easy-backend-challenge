'use strict';

module.exports = {
  up: (q, Sequelize) => q.sequelize.transaction(transaction => {
    return Promise.all([
      q.addColumn('partnerServices', 'partnerId', {
        type: Sequelize.STRING
      }, { transaction }),
      q.addColumn('partnerServices', 'serviceId', {
        type: Sequelize.INTEGER
      }, { transaction }),
    ])
  }),

  down: q => q.sequelize.transaction(transaction => {
    return Promise.all([
      q.removeColumn('partnerServices', 'serviceId', { transaction }),
      q.removeColumn('partnerServices', 'partnerId', { transaction }),
    ])
  })
};
