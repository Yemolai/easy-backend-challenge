require('dotenv').config()

const {
  DATABASE_HOST: host,
  DATABASE_PORT: port,
  DATABASE_USER: username,
  DATABASE_PASS: password,
  DATABASE_NAME: database = 'postgres'
} = process.env

const { DATABASE_TYPE: dialect = 'postgres' } = process.env

const connectionParameters = { host, port, username, password, database }

const controlTables = {
  migrationStorage: 'sequelize',
  seederStorage: 'sequelize',
  seederStorageTableName: 'sequelize_seeders',
  migrationStorageTableName: 'sequelize_migrations'
}

module.exports = {
  development: {
    ...connectionParameters,
    database: 'development',
    ...controlTables,
    dialect
  },
  test: {
    ...connectionParameters,
    database: 'test',
    ...controlTables,
    dialect
  },
  production: {
    ...connectionParameters,
    ...controlTables,
    dialect
  }
}
