const servicesFile = require('../seedData/services.json')
const services = servicesFile.map(name => ({ name }))
module.exports = {
  up: q => q.bulkInsert('services', services),
  down: q => q.bulkDelete('services', null, {})
}
