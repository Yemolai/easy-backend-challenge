const tableName = 'users'
const isoNow = new Date().toISOString()
const users = require('../seedData/users.json')
  .map(user => ({
    ...user,
    email: `${user.email}`.toLocaleLowerCase(),
    createdAt: isoNow,
    updatedAt: isoNow
  }))
module.exports = {
  up: (q, _) => q.bulkInsert(tableName, users, {})
    .catch(err => {
      console.error(err.message, err)
      throw err
    }),
  down: (q, _) => q.bulkDelete(tableName, null, {})
    .catch(err => {
      console.error(err.message, err)
      throw err
    })
};
