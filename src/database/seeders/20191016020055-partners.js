const partnersFile = require('../seedData/partners.json')
const servicesFile = require('../seedData/services.json')
const addresses = require('../seedData/locations.json')
const now = new Date().toISOString()
const timestamps = { createdAt: now, updatedAt: now }
const partners = partnersFile.map(partner => ({
  ...partner,
  ...timestamps
}))

const locations = partners
  .map(partner => partner.location)
  .concat(addresses)

const locationsMap = new Map(
  locations
    .map((location, k) => [
      location.address,
      { ...location, id: k + 1, ...timestamps }
    ])
)

const services = servicesFile.reduce((acc, name, k) => {
  const serviceName = `${name}`.trim()
  const newService = { [serviceName]: k + 1 }
  return Object.assign({}, acc, newService)
}, {})

const partnerServices = partners
  .map(({ id, availableServices }) => availableServices
    .map(service => ({
      partnerId: id,
      serviceId: services[service]
    }))
  )
  .reduce((acc, arr) => [...acc, ...arr], [])
  .map(partnerService => ({
    ...partnerService,
    ...timestamps
  }))

module.exports = {
  up: (queryInterface, _Sequelize) => {
    return queryInterface.sequelize.transaction(async transaction => {
      await queryInterface
        .bulkInsert('locations', Array.from(locationsMap.values()), { transaction })
        .catch(err => {
          console.error('Failed to insert locations', err.message, err)
          throw err
        })
      const associatedPartners = partners.map(partner => {
        const associatedLocation = locationsMap.get(partner.location.address)
        if (!associatedLocation) {
          console.error({ associatedLocation, partner })
          throw new Error('Failed to find associated location')
        }
        const newPartner = {
          ...partner,
          locationId: associatedLocation.id
        }
        delete newPartner.location
        return newPartner
      })
      await queryInterface.bulkInsert('partners', associatedPartners, { transaction })
      await queryInterface.bulkInsert('partnerServices', partnerServices, { transaction })
    })
  },

  down: (queryInterface, _Sequelize) => {
    return queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.bulkDelete('partnerServices', null, { transaction })
      await queryInterface.bulkDelete('partners', null, { transaction })
      return queryInterface.bulkDelete('locations', null, { transaction })
    })
  }
}
