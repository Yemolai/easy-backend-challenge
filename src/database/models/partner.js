module.exports = (sequelize, DataTypes) => {
  const partner = sequelize.define('partner', {
    id: {
      type: DataTypes.STRING,
      primaryKey: true
    },
    name: DataTypes.STRING,
    locationId: {
      type: DataTypes.INTEGER,
      references: {
        model: {
          tableName: 'locations',
          key: 'id'
        }
      }
    }
  }, {
    timestamps: true
  })
  partner.associate = function(models) {
    const { location, service } = models
    location.hasMany(partner)
    partner.belongsTo(location)
    partner.belongsToMany(service, {
      as: 'availableServices',
      through: 'partnerServices',
      foreignKey: 'partnerId'
    })
  }
  return partner
}
