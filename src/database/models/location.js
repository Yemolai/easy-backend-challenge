module.exports = (sequelize, DataTypes) => {
  const location = sequelize.define('location', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    name: DataTypes.STRING,
    address: DataTypes.STRING,
    city: DataTypes.STRING,
    state: DataTypes.STRING,
    country: DataTypes.STRING,
    lat: DataTypes.NUMBER,
    long: DataTypes.NUMBER
  }, {
    timestamps: true
  });
  location.associate = function(models) {
    // associations can be defined here
  };
  return location;
};
