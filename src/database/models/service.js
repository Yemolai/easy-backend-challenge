'use strict';
module.exports = (sequelize, DataTypes) => {
  const service = sequelize.define('service', {
    id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
    name: DataTypes.STRING
  }, {
    timestamps: false
  });
  service.associate = function(models) {
    const { partner } = models
    service.belongsToMany(partner, {
      through: 'partnerServices',
      foreignKey: 'serviceId'
    })
  };
  return service;
};
