FROM node:lts-slim

# Create app dir
WORKDIR /app

# Copy package dependencies configuration
COPY package.json ./
COPY yarn.lock ./

# Install production dependencies
RUN npx yarn --prod

# Copy app scripts
COPY . .

# Define exposed port
EXPOSE 8080

# Starts with app's start command defined on package.json
CMD ["npm", "start"]
