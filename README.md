# Easy Carros Backend Challenge

This is an application written to attend a technical test. The goal is to develop a backend API.

## Installation

Its dependencies can be installed using the [`yarn`](https://yarnpkg.com/en/) command line tool.

```sh
yarn
```

It can be run with the default `start` script given the `.env` file is present to run locally.

```sh
# NPM
npm start
# Or using Yarn
yarn start
```

It can be executed on docker using the given `Dockerfile` to build the image or the `docker-compose.yml`.

## Environment variables

An example file `.env.example` is provided. The default required `.env` should be like:

```env
NODE_ENV=production
JWT_SECRET=your-jwt-secret-to-hash-the-token
DATABASE_HOST=localhost
DATABASE_PORT=5432
DATABASE_USER=postgres
DATABASE_PASS=docker
GMAPS_TOKEN=your-google-maps-api-key-for-geocoding-usage
```

The database environment values in the `docker-compose.yml` are predefined to connect to the linked database container. So the .env to use the docker image should be like:

```env
JWT_SECRET=your-jwt-secret-to-hash-the-token
GMAPS_TOKEN=your-google-maps-api-key-for-geocoding-usage
```

### Execution

The root route redirects to a swagger ui that documents the code usage. The three stories to implement are present on the following routes:

  - #1 `/login`
  - #2 `/services/{service}/partners/nearest`
  - #3 `/partners/near`

Examples of arguments to use the routes are on the swagger/OpenAPI documentation.
The authentication of the routes uses JWT and can be found in swagger to use.
This application is an example so by default the given data in the `users.json` file is valid to login, just double-check to write the email of the user all in lowercase.

The #3 route usage requires an `GMAPS_TOKEN` to be used, so be sure to provide a Google Maps Javascript API Token with Geocoding API enabled, you can create one [here](), before running.

## Tests

To run the test suite, first install the dependencies, then run `test`:

```sh
# NPM
npm test
# Or Using Yarn
yarn test
```

## Dependencies

- [axios](https://ghub.io/axios): Promise based HTTP client for the browser and node.js
- [bcrypt](https://ghub.io/bcrypt): A bcrypt library for NodeJS.
- [cookie-parser](https://ghub.io/cookie-parser): Parse HTTP request cookies
- [cors](https://ghub.io/cors): Node.js CORS middleware
- [cuid](https://ghub.io/cuid): Collision-resistant ids optimized for horizontal scaling and performance. For node and browsers.
- [debug](https://ghub.io/debug): small debugging utility
- [dotenv](https://ghub.io/dotenv): Loads environment variables from .env file
- [express](https://ghub.io/express): Fast, unopinionated, minimalist web framework
- [express-jwt](https://ghub.io/express-jwt): JWT authentication middleware.
- [geolib](https://ghub.io/geolib): [![CircleCI](https://circleci.com/gh/manuelbieh/geolib/tree/master.svg?style&#x3D;svg)](https://circleci.com/gh/manuelbieh/geolib/tree/master) ![](https://badgen.net/bundlephobia/minzip/geolib) ![](https://badgen.net/npm/dm/geolib)
- [jsonwebtoken](https://ghub.io/jsonwebtoken): JSON Web Token implementation (symmetric and asymmetric)
- [morgan](https://ghub.io/morgan): HTTP request logger middleware for node.js
- [pg](https://ghub.io/pg): PostgreSQL client - pure javascript &amp; libpq with the same API
- [sequelize](https://ghub.io/sequelize): Multi dialect ORM for Node.JS
- [sequelize-cli](https://ghub.io/sequelize-cli): The Sequelize CLI
- [swagger-jsdoc](https://ghub.io/swagger-jsdoc): Generates swagger doc based on JSDoc
- [swagger-ui-express](https://ghub.io/swagger-ui-express): Swagger UI Express

## Dev Dependencies

- [chai](https://ghub.io/chai): BDD/TDD assertion library for node.js and the browser. Test framework agnostic.
- [faker](https://ghub.io/faker): Generate massive amounts of fake contextual data
- [mocha](https://ghub.io/mocha): simple, flexible, fun test framework
- [nodemon](https://ghub.io/nodemon): Simple monitor script for use during development of a node.js app.

## Contributors

Pull requests and stars are always welcome. For bugs and feature requests, please [create an issue](https://github.com/user/repo/issues). [List of all contributors](https://github.com/user/repo/graphs/contributors).
