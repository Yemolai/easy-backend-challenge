/* global describe, it, beforeEach, afterEach */
/* eslint-disable no-unused-expressions */
process.env.NODE_ENV = 'test'
const { expect } = require('chai')
const { sequelize } = require('../../src/database')

const userCorrectFixture = {
  id: '1',
  name: 'admin',
  email: 'admin@company.com',
  password: 'admin123'
}

describe('Model user', () => {
  const { user: UserModel } = sequelize.models
  beforeEach(async () => {
    await UserModel.sync({ force: true })
  })
  afterEach(async () => {
    await UserModel.drop()
  })
  it('should create', async () => {
    const user = UserModel.create(userCorrectFixture)
    expect(user).to.exist
  })
  it('should create and save', async () => {
    const user = await UserModel.create(userCorrectFixture)
      .save()
    expect(user).to.exist
      .and.to.haveOwnProperty('createdAt')
  })
})
